package com.example.task06;

public class Task06 {

    public static int solution(int x, int y) {
        int temp = x + y,
                counter = 0;
        while (true) {
            temp = temp / 10;
            counter++;

            if (temp == 0)
                break;
        }
        return counter;
    }

    public static void main(String[] args) {
        // Здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        /*
        int result = solution(12, 34);
        System.out.println(result);
        */
    }

}
