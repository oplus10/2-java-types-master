package com.example.task02;

public class Task02 {

    public static String solution(String input) {
        try {
            long value = Long.parseLong(input);
            if (value >= Byte.MIN_VALUE && value <= Byte.MAX_VALUE)
                return "byte";
            else if (value >= Short.MIN_VALUE && value <= Short.MAX_VALUE)
                return "short";
            else if (value >= Integer.MIN_VALUE && value <= Integer.MAX_VALUE)
                return "int";
            else if (value >= Long.MIN_VALUE && value <= Long.MAX_VALUE)
                return "long";
        } catch (NumberFormatException e) {
            return "Unknown type!";
        }
        return "";
    }

    public static void main(String[] args) {
        // Здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        /*
        String result = solution("12345");
        System.out.println(result);
         */
    }

}
