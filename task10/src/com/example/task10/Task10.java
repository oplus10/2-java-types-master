package com.example.task10;

public class Task10 {

    public static boolean compare(float a, float b, int precision) {
        if ((Float.isInfinite(a) || Float.isNaN(a)) && (!Float.isInfinite(b) && !Float.isNaN(b)))
            return false;
        else if ((Float.isInfinite(b) || Float.isNaN(b)) && (!Float.isInfinite(a) && !Float.isNaN(a)))
            return false;
        else if ((a == Float.NEGATIVE_INFINITY && b == Float.POSITIVE_INFINITY) || (a == Float.POSITIVE_INFINITY && b == Float.NEGATIVE_INFINITY))
            return false;
        else if ((Float.isInfinite(a) && Float.isInfinite(b)) || (Float.isNaN(a) && Float.isNaN(b)))
            return true;

        float epsilon = 1;
        for (int i = 0; i < precision; i++) {
            epsilon = epsilon / 10;
        }

        return Math.abs(a - b) < epsilon;
    }

    public static void main(String[] args) {
        float a = 1.0f;
        float b = 2.0f;
        float sum = a + b;
        float c = 0.5f;

        boolean result = compare(a, b, 10);
        System.out.println(result);

    }

}
